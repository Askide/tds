// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FunctionLibrary/Types.h"
#include "../Weapons/WeaponDefault.h"
#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:
	ATPSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UDecalComponent* CurrentCursor = nullptr;

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();
	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeedInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	
	// Tick Func
	UFUNCTION()
		void MovementTick(float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();
	bool CanSprint;

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	
	//Inputs
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	
	//Camera
	UFUNCTION()
		void CameraZooming(float Value);
	UFUNCTION()
		void CameraZoomingTimer();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HeightCameraChangeTickDistance = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HeightCameraChangeDistance = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HeightCameraMin = 700.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float HeightCameraMax = 1000.0f;
	
	bool bZoomChanging = false;
	bool bZoomUp = false;
	
	float fNewTickDistance = 0;
	float fCurrentTickDistance = 0;

	FTimerHandle CameraZoom;

	//Weapon	
	AWeaponDefault* CurrentWeapon = nullptr;

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IDWeapon);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		FName InitWeaponName;
	
	//Attack
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);

	//Delegates Implementation
	UFUNCTION()
		void CharAttack(UAnimMontage* CharAnim);
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* CharAnim);
	UFUNCTION()
		void WeaponReloadEnd();
	

	UFUNCTION(BlueprintNativeEvent)
		void CharAttack_BP(UAnimMontage* CharAnim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* CharAnim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP();
	
};



